---
layout: page
title: About
permalink: /about/
---

I'm Josh, an IT Professional from Milton Keynes in the United Kingdom.

I've been testing software now for 5 years and recently, I've started to take learning to code more seriously.

Here you'll find my ramblings about a whole range of topics - the IT industry, stuff I've been learning or working on and retro games.

Why not visit my website for contact information? Click [here][here]!

[here]: https://joshblewitt.dev/